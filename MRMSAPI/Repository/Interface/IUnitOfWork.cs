﻿namespace MRMSAPI.Repository.Interface
{
    public interface IUnitOfWork
    {
        IOfficeRepository Offices { get; }
    }
}
