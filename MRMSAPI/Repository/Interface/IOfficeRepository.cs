﻿using MRMSAPI.API.ParamModel;
using Shared.Interfaces;

namespace MRMSAPI.Repository.Interface
{
    public interface IOfficeRepository : IGenericRepository<OfficeModel>
    {
    }
}
