﻿using MRMSAPI.Repository.Interface;

namespace MRMSAPI.Repository.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IOfficeRepository officeRepository)
        {
            Offices = officeRepository;
        }

        public IOfficeRepository Offices { get; }
    }
}
