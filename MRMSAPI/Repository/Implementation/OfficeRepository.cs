﻿using Dapper;
using Microsoft.Data.SqlClient;
using MRMSAPI.API.ParamModel;
using MRMSAPI.Repository.Interface;

namespace MRMSAPI.Repository.Implementation
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly IConfiguration _configuration;

        public OfficeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Task<int> AddAsync(OfficeModel entity)
        {
            throw new NotImplementedException();
        }

        public Task<int> DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyList<OfficeModel>> GetAllAsync()
        {
            var sql = "SELECT * FROM tblOffice";
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<OfficeModel>(sql);
                return result.ToList(); 
            }
        }

        public Task<OfficeModel> GetByIdAsync(int Id)
        {
            throw new NotImplementedException();
        }

        public Task<int> UpdateAsync(OfficeModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
