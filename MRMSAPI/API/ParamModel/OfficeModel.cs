﻿namespace MRMSAPI.API.ParamModel
{
    public class OfficeModel
    {
        public string OfficeID { get; set; } =string.Empty;
        public string OfficeName { get; set;} = string.Empty;
        public string Location { get; set;} = string.Empty;
        public int Status { get; set;}
    }
}
