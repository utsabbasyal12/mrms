﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MRMSAPI.Repository.Interface;

namespace MRMSAPI.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeAPIController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public OfficeAPIController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var data = await _unitOfWork.Offices.GetAllAsync();
            return Ok(data);
        }
    }
}
