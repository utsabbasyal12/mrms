﻿using Microsoft.EntityFrameworkCore;
using MRMSAPI.Data;
using MRMSAPI.Repository.Implementation;
using MRMSAPI.Repository.Interface;

namespace MRMSAPI.Configuration.Extension
{
    public static class ServiceConfiguration
    {
        public static void AddCustomService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IOfficeRepository, OfficeRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
